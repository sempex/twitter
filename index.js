import { getData } from "./lib/getData.js";
import { simplifyBlocks } from "./lib/simplifyBlocks.js";
import schedule from "node-schedule";
import moment from "moment";
import { sendTweet } from "./lib/sendTweet.js";


const sampleData = {
  lessons: [
    {
      startTimeValue: "22:47",
      name: "Mashalla!",
    },
    {
      startTimeValue: "22:48",
      name: "Mashalla! Einfach die Besten Coder!",
    },
  ],
};

const main = async () => {
  schedule.scheduleJob('20 4 * * *', () => {
    scheduleTask()
  })

  const scheduleTask = async () => {
    //const data = await getData(moment().format("YYYYMMDD"));
    const data = await getData('20220513')
    const blocks = data.blocks;
    const simpleData = simplifyBlocks(blocks);  
    console.log(`Received ${simpleData.lessons.length} lessons for today.`);
    simpleData.lessons.forEach((lesson) => {
      const rule = new schedule.RecurrenceRule();
      const [hour, minute] = lesson.startTimeValue.split(":");
      rule.dayOfWeek = [new schedule.Range(4-6)];
      rule.hour = hour;
      rule.minute = minute - 15;
      schedule.scheduleJob(rule, () => {
        console.log(lesson);
        sendTweet("Your Script Worked", lesson)
      });
    });
  }
 
};

main();
