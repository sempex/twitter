import puppeteer from "puppeteer";

async function getData(date) {
  const browser = await puppeteer.launch();
  const page = await browser.newPage();

  await page.goto(
    "https://tipo.webuntis.com/WebUntis/index.do?school=GIBB#/basic/timetable"
  );
  await page.goto(
    `https://tipo.webuntis.com/WebUntis/api/public/period/info?date=${date}&starttime=715&endtime=1645&elemid=22574&elemtype=1`,
    { waitUntil: "networkidle0" }
  );

  const res = await page.evaluate(
    () => document.querySelector("pre").innerHTML
  );
  await browser.close();

  return JSON.parse(res).data;
}

export { getData };
