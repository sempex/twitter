import { TwitterClient } from "twitter-api-client";
import 'dotenv/config'



const twitterClient = new TwitterClient ({
    apiKey: process.env.TWITTER_API_KEY,
    apiSecret: process.env.TWITTER_API_SECRET,
    accessToken: process.env.TWITTER_ACCESS_TOKEN,
    accessTokenSecret: process.env.TWITTER_ACCESS_TOKEN_SECRET
})

const sendTweet = (msg) => {
    twitterClient.tweets.statusesUpdate({
        status: msg
    }).then(response => {
        console.log("Tweeted!: ", msg, "Response: ", response)
    }).catch(err => {
        console.log(err)
    })
}

export { sendTweet }