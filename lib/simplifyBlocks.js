import moment from "moment";
function simplifyBlocks(blocks) {
  let simpleData = { lessons: [] };

  blocks.forEach((block) => {
    block.forEach((lessons) => {
      const lesson = lessons.periods[0];
      const { startTime, endTime, teachers, rooms } = lesson;
      const [teacher] = teachers;
      const [room] = rooms;
      const teacherName = teacher.name;
      const roomName = room.longName;

      const startTimeValue = moment(
        startTime,
        startTime.toString().length === 3 ? "Hmm" : "HHmm"
      ).format("HH:mm");
      const endTimeValue = moment(
        endTime,
        endTime.toString().length === 3 ? "Hmm" : "HHmm"
      ).format("HH:mm");
      simpleData.lessons.push({
        startTimeValue,
        endTimeValue,
        roomName,
        teacherName,
      });
    });
  });

  return simpleData;
}

export { simplifyBlocks };
